<h1>Software Ryze</h1>
O Software Ryze é uma plataforma que consiste em uma caderneta de campo eletrônica, a qual possibilita o registro de informações e posições, assim como a visualização das mesmas processadas.
<img src="Software_Ryze.png" />

Sendo desenvolvido na plataforma GitLab, ele é composto de 67,5% de Javascript (JS), 20,2% de HyperText Markup Language (HTML) e 12,3% de Cascading Style Sheet (CSS), os quais estão devidamente distribuídos em 10 arquivos de código, além de diversos arquivos de imagem e auxiliares; A elaboração dos códigos foram feitas na ferramenta da plataforma “Web IDE”, dado a simplicidade. As bibliotecas utilizadas foram:
- JQuery: Para fins de simplificação de código, substituindo a DOM em determinados momentos para o código ficar mais simples e conciso (https://jquery.com/)
- CanvasJS: Para a criação de gráficos (Perfil) com o método “Spline” de interpolação, representado um perfil curvas suaves e diversas ferramentas como exportação de gráfico e interação com os pontos (https://canvasjs.com/)
- Leaflet: Para o mapeamento de localizações, de modo a ser possível navegar com o mapa da região e visualizar as posições informadas (https://leafletjs.com/)

# Páginas
O sistema conta com 3 páginas navegáveis entre si (as quais estão apresentadas em anexo), sendo elas:
- Menu Principal
- Página de cadernetas
- Resultados

# Backlog
### Design
- **Melhoria no Design do programa em geral**, sem perder a identidade apresentada, de modo a deixar o programa mais bonito e profissionais, sendo visualmente mais agradável aos usuários.
- **Visualização compatível com o mobile**, a fim de melhorar a experiência de uso em aparelhos celulares, smartphones e tablets

### Estrutural
- **Melhoria na navegação de páginas**.
- **Novas cadernetas de nivelamento** para os níveis digitais, geodésicos ópticos e geodésicos digitais.

### Funcionalidades
- **Importação e exportação de projetos**, a fim de salvar os registros realizados e carregar diversos levantamentos (inclusive realizados por outras pessoas).
- **Exportação de relatórios do projeto**, a fim de ser um produto que contenha as mais diversas informações.

### Cálculos
- **Implementação do ajustamento de observações**.
- **Realização de cálculos resultantes como erro de fechamento e correções a serem aplicadas**.

### Mapas
- **Visualização de RN's** através WMS e WFS.
- **Visualização da localização dos pontos observados** através de métodos de interpolação.



