<h1>Menu Principal</h1>
Esta página é utilizada para definição de nome do projeto e o tipo de nível a ser utilizado, dado que cada tipo de nível está associado com uma caderneta diferente, além de contar com uma guia de importação de projeto para fins de continuidade de trabalho, independentemente do dispositivo.

No momento, apenas estão aplicadas as funcionalidades de inserção de nome do projeto e escolha do tipo do equipamento. O único tipo de caderneta disponível no momento é para o nível óptico, mas em versões futuras vão ser adicionadas as cadernetas referentes aos níveis digitais, geodésicos ópticos e geodésicos digitais. Além disso, em atualizações futuras será possível realizar a importação de projetos, função não implementada até o momento.

