function showContentTab(tab){
  //Instanciar variaveis principais
  var tabs = document.getElementById("menuTab").children;
  var divs = document.getElementsByClassName("caderneta");
  var help = false;
  
  //Destacar o levantamento selecionado
  for (i = 0; i < tabs.length; i++){
    var principal = "black";
    var secundario = "#F8F8F8";
    var borda = "none";
    if (tabs[i].id == "tabhelp") var image = "images/interrogation_white";
    if (tabs[i] == tab){
      levantamento = i;
      principal = "#F8F8F8";
      secundario = "black";
      borda = "3px solid LightSteelBlue";
      if (tabs[i].getElementsByTagName("IMG").length == 1){
        image = "images/interrogation_black";
        help = true;
      }
    }
    if (tabs[i].getElementsByTagName("INPUT").length == 1){
      tabs[i].children[0].style.background = principal;
      tabs[i].children[0].style.color = secundario;
    }
    tabs[i].style.background = principal;
    tabs[i].style.color = secundario;
    tabs[i].style.borderBottom = borda;
    if (tabs[i].id == "tabhelp") tabs[i].children[0].src = image;
  }
  
  //Mostrar o conteudo do levantamento selecionado
  for (i = 0; i < divs.length; i++){
    divs[i].style.display = (i === levantamento && help === false) ? "block":"none";
  }
  if (help) divs[divs.length - 1].style.display = "block";
}
      
function addTab(nameDefault=null){
  //Instanciar variaveis principais
  var menu = document.getElementById("menuTab");
  var numTabs = menu.children.length - 1;
  var body = document.getElementById("info");
  
  //Criar elementos
  var newTable = document.createElement("TABLE");
  var newTab = document.createElement("BUTTON");
  
  //Configurar TABLE
  newTable.className = "caderneta";
  newTable.id = "niv" + (numTabs - 1);
  newTable.border = "1";
  newTable.innerHTML = "\
    <col width='10%'>\
    <col width='10%'>\
    <col width='15%'>\
    <col width='12.5%'>\
    <col width='12.5%'>\
    <col width='15%'>\
    <col width='20%'>\
    <thead class='header'>\
      <th width='10%'>RE</th>\
      <th width='10%'>VANTE</th>\
      <th width='15%'>DISTANCIA RE</th>\
      <th width='12.5%'>LEITURAS RE</th>\
      <th width='12.5%'>LEITURAS VANTE</th>\
      <th width='15%'>DISTANCIA VANTE</th>\
      <th width='20%'>DESNIVEL</th>\
    </thead>\
    <tbody>\
      <tr>\
        <td rowspan='3' class='data' width='10%'><input type='text' height='100%'></td>\
        <td rowspan='3' class='data' width='10%'><input type='text' height='100%'></td>\
        <td rowspan='3' class='data' width='15%'><label id='dist'></label></td>\
        <td class='data' width='12.5%'><input type='number' oninput='this.value = this.value;'></td>\
        <td class='data' width='12.5%'><input type='number' oninput='this.value = this.value;'></td>\
        <td rowspan='3' class='data' width='15%'><label id='dist'></label></td>\
        <td rowspan='3' class='data' width='20%'><label id='desnivel'></label></td>\
        <td rowspan='3' onclick='verify(this);'><img src='images/verificar' width=25></td>\
      </tr>\
      <tr>\
        <td class='data' width='12.5%'><input type='number' oninput='this.value = this.value;'></td>\
        <td class='data' width='12.5%'><input type='number' oninput='this.value = this.value;'></td>\
      </tr>\
      <tr>\
        <td class='data' width='12.5%'><input type='number' oninput='this.value = this.value;'></td>\
        <td class='data' width='12.5%'><input type='number' oninput='this.value = this.value;'></td>\
      </tr>\
    </tbody>";
  
  //Configurar tab
  if (nameDefault == null){
    var tabs = 0;
    for (i = 0; i < (menu.children.length - 2); i++){
      var name = "New Tab";
      if (menu.children[i].children[0].value.match(/New Tab/)){
        if (tabs != 0) name += " (" + tabs + ")";
        menu.children[i].children[0].value = name;
        tabs++;
      }
    }
    if (tabs != 0) name = "New Tab (" + tabs + ")";
  }
  else name = nameDefault;
  newTab.className = "tabs";
  newTab.id = "tab" + (numTabs - 1);
  newTab.onclick = function() {showContentTab(this)};
  newTab.onfocus = function() {this.addEventListener('click', function () {this.children[0].disabled = false;})};
  newTab.addEventListener("focusout", function() {this.children[0].disabled = true;});
  newTab.innerHTML = "<input value='" + name + "' disabled=" + true + ">";
  
  //Adicionar os elementos criados a pagina
  body.insertBefore(newTable, document.getElementById("nivhelp"));
  menu.insertBefore(newTab, document.getElementById("tabadd"));

  //Selecionar o novo levantamento
  showContentTab(newTab);
}

function deleteTab(tab){
  //Instanciar variaveis principais
  var tables = document.getElementsByClassName("caderneta");
  var menuTab = document.getElementById("menuTab");
  
  //Remover aba e caderneta do levantamento em questao
  document.getElementById("info").removeChild(tables[tab]);
  menuTab.removeChild(menuTab.children[tab]);
  
  //Adicionar aba em casos onde ha uma lista vazia
  if (menuTab.children.length == 2) addTab(nameDefault="Nivelamento");
}

function addObservation(){
  //Instanciar variavel principal
  var tables = document.getElementsByClassName("caderneta");
  var help = false;
  for (i = 0; i < tables.length; i++){
    if (tables[i].style.display == "block"){
      if (tables[i].id == "nivhelp") help = true;
      var tbody = tables[i].getElementsByTagName("TBODY")[0];
      break;
    }
  }
  
  //Metodo alternativo para aba de ajuda
  if (help){
    alert("Este botao serve para adicionar uma nova observacao de lance de acordo com o padrao estabelecido acima.");
    return;
  }
  
  //Criar novas linhas
  var trp = tbody.insertRow();
  var tr1 = tbody.insertRow();
  var tr2 = tbody.insertRow();
  
  //Instanciar linha principal
  var re = trp.insertCell();
  re.setAttribute("rowspan", "3");
  re.setAttribute("class", "data");
  re.setAttribute("width", "10%");
  re.innerHTML ="<input type='text' height='100%'>";
  var vante = trp.insertCell();
  vante.setAttribute("rowspan", "3");
  vante.setAttribute("class", "data");
  vante.setAttribute("width", "10%");
  vante.innerHTML ="<input type='text' height='100%'>";
  var distR = trp.insertCell();
  distR.setAttribute("rowspan", "3");
  distR.setAttribute("class", "data");
  distR.setAttribute("width", "15%");
  distR.innerHTML ="<label id='dist'></label>";
  var fsR = trp.insertCell();
  fsR.setAttribute("class", "data");
  fsR.setAttribute("width", "12.5%");
  fsR.innerHTML ="<input type='number' oninput='this.value = this.value;'>";
  var fsV = trp.insertCell();
  fsV.setAttribute("class", "data");
  fsV.setAttribute("width", "12.5%");
  fsV.innerHTML ="<input type='number' oninput='this.value = this.value;'>";
  var distV = trp.insertCell();
  distV.setAttribute("rowspan", "3");
  distV.setAttribute("class", "data");
  distV.setAttribute("width", "15%");
  distV.innerHTML ="<label id='dist'></label>";
  var dsnv = trp.insertCell();
  dsnv.setAttribute("rowspan", "3");
  dsnv.setAttribute("class", "data");
  dsnv.setAttribute("width", "20%");
  dsnv.innerHTML ="<label id='dist'></label>";
  var ver = trp.insertCell();
  ver.setAttribute("rowspan", "3");
  ver.setAttribute("onclick", "verify(this);");
  ver.innerHTML = "<img src='images/verificar' width=25>";

  //Instanciar linhas secundarias
  var fmR = tr1.insertCell();
  fmR.setAttribute("class", "data");
  fmR.setAttribute("width", "12.5%");
  fmR.innerHTML ="<input type='number' oninput='this.value = this.value;'>";
  var fmV = tr1.insertCell();
  fmV.setAttribute("class", "data");
  fmV.setAttribute("width", "12.5%");
  fmV.innerHTML ="<input type='number' oninput='this.value = this.value;'>";
  var fiR = tr2.insertCell();
  fiR.setAttribute("class", "data");
  fiR.setAttribute("width", "12.5%");
  fiR.innerHTML ="<input type='number' oninput='this.value = this.value;'>";
  var fiV = tr2.insertCell();
  fiV.setAttribute("class", "data");
  fiV.setAttribute("width", "12.5%");
  fiV.innerHTML ="<input type='number' oninput='this.value = this.value;'>";
}
