function calcDist(rows, ori, alerta="yes"){
  //Definir variaveis auxiliares de acordo com a orientaçao do equipamento
  if (ori == "re"){
    leitS = 3;
    leitMI = 0;
    dist = 2;
  }
  if (ori == "vante"){
    leitS = 4;
    leitMI = 1;
    dist = 5;
  }
  
  //Instanciar variaveis principais
  var fs = parseFloat(rows[0].cells[leitS].children[0].value);
  var fi = parseFloat(rows[2].cells[leitMI].children[0].value);

  //Calcular distancia
  var distancia = Math.abs((fs - fi) * 100);
  
  //Gravar resultado
  rows[0].cells[dist].children[0].innerHTML = distancia.toFixed(3) + " m";
  
  //Verificar erros
  if ("" + fs == "NaN"){
    rows[0].cells[dist].children[0].innerHTML = "";
    if (alerta == "yes") alert("Leitura do fio superior (orientado a " + ori + ") nao foi reconhecido");
  }
  if ("" + fi == "NaN"){
    rows[0].cells[dist].children[0].innerHTML = "";
    if (alerta == "yes") alert("Leitura do fio inferior (orientado a " + ori + ") nao foi reconhecido");
  }
}

function verifyMed(rows){
  //Instanciar variaveis principais
  var fsr = parseFloat(rows[0].cells[3].children[0].value);
  var fir = parseFloat(rows[2].cells[0].children[0].value);
  var fsv = parseFloat(rows[0].cells[4].children[0].value);
  var fiv = parseFloat(rows[2].cells[1].children[0].value);
  var re = rows[1].cells[0].children[0];
  var vante = rows[1].cells[1].children[0];
  var verificado = true;
  
  //Comparar valor calculado com o registrado
  if (Math.abs((fsr + fir) / 2 - parseFloat(re.value)) < 0.003) {re.style.color = "green";}
  else {
    re.style.color = "red";
    verificado = false;
  }
  if (Math.abs((fsv + fiv) / 2 - parseFloat(vante.value)) < 0.003) {vante.style.color = "green";}
  else {
    vante.style.color = "red";
    verificado = false;
  }
  return verificado;
}

function calcDsnv(rows) {
  //Instanciar variaveis principais
  var re = parseFloat(rows[1].cells[0].children[0].value);
  var vante = parseFloat(rows[1].cells[1].children[0].value);
  
  //Calcular desnivel
  var desnivel = re - vante;
  
  //Gravar resultado
  rows[0].cells[6].children[0].innerHTML = desnivel.toFixed(3) + " m";
}

function block(rows){
  //Loop para bloquear inputs
  for (i = 0; i < 3; i++){
    for (j = 0; j < rows[i].cells.length; j++){
      if (rows[i].cells[j].getElementsByTagName("INPUT").length == 1) rows[i].cells[j].children[0].disabled = true;
    }
  }
  
  //Alterar icone de verificaçao para remoçao
  rows[0].cells[7].children[0].src = "images/remove";
  rows[0].cells[7].onclick = function(){removeRow(rows[0]);};
}

function verify(element, alerta="yes", $geolocation=true) {
  //Instanciar variaveis principais
  let tbody = element.parentElement.parentElement;
  for (i = 0; i < tbody.rows.length; i++){
    if (tbody.rows[i] == element.parentElement){
      var rows = [
        tbody.rows[i],
        tbody.rows[i + 1],
        tbody.rows[i + 2]
      ];
      break;
    }
  }
  
  //Iniciar metodos de calculos e verificaçoes
  calcDist(rows, "re", alerta=alerta);
  calcDist(rows, "vante", alerta=alerta);
  verificacao = verifyMed(rows);
  if (verificacao == false){
    rows[0].cells[6].children[0].innerHTML = "";
    if (alerta=="yes") alert("Erro nas leituras, confira os valores inseridos");
    return;
  }
  calcDsnv(rows);
  if ($geolocation) {
    addLocation($("#tab" + tbody.parentElement.id.split('niv')[1]).children().first().val().replace(/ /g, '_'));
  }
  block(rows);
}

function removeRow(row, alerta="yes"){
  //Confirmar remoçao
  var resposta = true;
  if (alerta == "yes") resposta = confirm("Realmente deseja remover a linha criada?");
  if (resposta == true){
    //Iniciar remoçao
    var tbody = row.parentElement;
    for (i = 0; i < tbody.rows.length; i++){
      if (tbody.rows[i] == row){
        tbody.deleteRow(i + 2);
        tbody.deleteRow(i + 1);
        tbody.deleteRow(i);
        break;
      }
    }
    //Adicionar linha em casos onde ha uma lista vazia
    if (tbody.rows.length == 0) addObservation(tbody);
  }
}
