function process(){
  //Instanciar variaveis principais
  window.results = []
  var tables = document.getElementsByClassName("caderneta");
  var tabs = document.getElementById("menuTab").children;
  var resultTable = document.getElementById("resultados").getElementsByTagName("TBODY")[0];
  
  //Confirmar processamento
  resposta = confirm("Realmente deseja continuar com o processamento? As linhas invalidas serao deletadas.");
  if (resposta == false) return;
  
  //Limpar tabela resultante
  resultTable.innerHTML = "";

  //Loop ao decorrer das cadernetas
  for (let i = (tables.length - 2); i >=0; i--){
    //Instanciar variaveis auxiliares para a caderneta
    let tbody = tables[i].getElementsByTagName("TBODY")[0],
        tab = tabs[i].children[0].value;

    //Calcular valores das observaçoes e remover linhas invalidas
    for (let j = parseInt((tbody.rows.length - 1) / 3); j >= 0; j--){
      verify(tbody.rows[j * 3].cells[7], alerta="no", $geolocation=false);
      if (tbody.rows[j * 3].cells[6].children[0].innerHTML == ""){
        tbody.deleteRow(j * 3 + 2);
        tbody.deleteRow(j * 3 + 1);
        tbody.deleteRow(j * 3);
      }
    }
    
    //Deletar levantamentos vazios
    if (tbody.rows.length == 0){
      deleteTab(i);
      continue;
    }
    
    //Definir a nomenclatura dos pontos para a identificaçao dos mesmos
    if (tbody.rows[0].cells[0].children[0].value == "") tbody.rows[0].cells[0].children[0].value = "0PP" + "_" + i;
    for (j = 0; j <= parseInt((tbody.rows.length - 1) / 3); j++){
      if (tbody.rows[j * 3].cells[0].children[0].value == "") tbody.rows[j * 3].cells[0].children[0].value = "P" + j + "_" + i;
      if (tbody.rows[j * 3].cells[1].children[0].value == "") tbody.rows[j * 3].cells[1].children[0].value = "P" + (j + 1) + "_" + i;
    }
    
    //Instanciar das variveis principais para a caderneta
    var minimo = 0,
        numObs = 0,
        distancias = [],
        distTot = 0,
        pontoInicial = tbody.rows[0].cells[0].children[0].value,
        pontoFinal,
        desnivel = 0,
        dataPoints = [
          {
            x: 0,
            y: 0,
            indexLabel: tbody.rows[0].cells[0].children[0].value,
            indexLabelFontStyle: "bold",
            toolTipContent: "<strong>" + tbody.rows[0].cells[0].children[0].value + "</strong><br>Altitude: " + minimo + " m",
            cursor: "pointer"
          }
        ];
    
    //Rodar ao decorrer das observaçoes e determinar grandezas relevantes
    for (let j = 0; j <= parseInt((tbody.rows.length - 1) / 3); j++){
      let aux_dist = parseFloat(tbody.rows[j * 3].cells[2].children[0].innerHTML) + parseFloat(tbody.rows[j * 3].cells[5].children[0].innerHTML);
      numObs += 1;
      distancias.push(aux_dist);
      distTot += aux_dist;
      pontoFinal = tbody.rows[j * 3].cells[1].children[0].value;
      desnivel += parseFloat(tbody.rows[j * 3].cells[6].children[0].innerHTML);
      
      dataPoints.push(
        {
          x: distTot,
          y: desnivel,
          indexLabel: pontoFinal,
          indexLabelFontStyle: "bold",
          toolTipContent: "<strong>" + pontoFinal + "</strong><br>Altitude: " + (minimo + desnivel) + " m",
          cursor: "pointer"
        }
      );
    }

    // Adicionar ao perfil
    window.results.push(
      {
        type: "splineArea",
        dataPoints: dataPoints,
        showInLegend: true,
        legendText: tab,
        highlightEnabled: false,
        bevelEnabled: true,
        fillOpacity: .5
      }
    );
  
    //Inserir e preencher linha referente ao levantamento bem-sucedido
    var row = resultTable.insertRow();
    row.insertCell().innerHTML = tab;
    row.insertCell().innerHTML = numObs;
    row.insertCell().innerHTML = (distancias.reduce((a, b) => a += b) / distancias.length / 1000).toFixed(3);
    row.insertCell().innerHTML = (distancias.reduce((a, b) => a += b) / 1000).toFixed(3);
    row.insertCell().innerHTML = pontoInicial;
    row.insertCell().innerHTML = pontoFinal;
    row.insertCell().innerHTML = desnivel.toFixed(3);
  }
  
  // saveJson(project);
  createProfile();
  showPosition();
  openResults();
}

function openResults(){
  var info = document.getElementById("info")
  info.parentElement.children[0].style.display = "none";
  info.parentElement.children[1].style.display = "none";
  info.parentElement.children[2].style.display = "none";
  $("#results").css("display", "block");
}

function closeResults(){
  var info = document.getElementById("info")
  $("#results").css("display", "none");
  $("#resultados").html(
      "<table border='1'>\
          <thead class='header'>\
            <th>Levantamento</th>\
            <th>Numero de observaçoes</th>\
            <th>Distancia Media (km)</th>\
            <th>Distancia Total (km)</th>\
            <th>Ponto Inicial</th>\
            <th>Ponto Final</th>\
            <th>Desnivel Total (m)</th>\
          </thead>\
          <tbody></tbody>\
        </table>\
        <div id='perfil'></div>\
        <div id='map'></div>"
  )
  info.parentElement.children[0].style.display = "block";
  info.parentElement.children[1].style.display = "block";
  info.parentElement.children[2].style.display = "block";
}

function saveJson(dict){
  //Verificar existencia de pasta referente ao programa
  var json = JSON.parse(window.file.read());
  json[dict["name"].toLowerCase()] = dict;
  window.file.delete();
  window.file.append(JSON.stringify(json));
  window.file.createNewFile();
}

function export2csv(){ 
  //Criar novo arquivo
  var tables = document.getElementsByClassName("caderneta");
  for (var i = 1; i < tables.length; i++){
    var tab = document.getElementById("menuTab").children[i].children[0].value;
    var file = new android.File(proj + "/" + tab + ".csv");
    var table = tables[i].getElementsByTagName("TBODY")[0];
    for (var j = 0; j <= parseInt((table.rows.length - 1) / 3); j++){
      //Definir cabeçalho
      text  = "RE, ";
      text += "VANTE, ";
      text += "DISTANCIA RE, ";
      text += "LEITURAS RE, ";
      text += "LEITURAS VANTE, ";
      text += "DISTANCIA VANTE, ";
      text += "DESNIVEL";
      file.append(text + "\n");
      
      //Definir conteudo das linhas
      var rows = [
        table.rows[j * 3],
        table.rows[j * 3 + 1],
        table.rows[j * 3 + 2]
      ]
      //Definir conteudo da linha 1
      text  = " , , , ";
      text += rows[0].cells[3].children[0].value + ", ";
      text += rows[0].cells[4].children[0].value + ", ";
      file.append(text + " , \n");
      //Definir conteudo da linha 2
      text  = rows[0].cells[0].children[0].value + ", ";
      text += rows[0].cells[1].children[0].value + ", ";
      text += rows[0].cells[2].children[0].innerHTML + ", ";
      text += rows[1].cells[0].children[0].value + ", ";
      text += rows[1].cells[1].children[0].value + ", ";
      text += rows[0].cells[5].children[0].innerHTML + ", ";
      text += rows[0].cells[6].children[0].innerHTML + ", ";
      file.append(text + "\n");
      //Definir conteudo da linha 3
      text  = " , , , ";
      text += rows[2].cells[0].children[0].value + ", ";
      text += rows[2].cells[1].children[0].value + ", ";
      file.append(text + " , \n");
    }
  }  alert("Arquivos salvos com sucesso.")
}
