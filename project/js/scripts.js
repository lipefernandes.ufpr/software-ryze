function onload() {
  //Atribuir informaçoes do projeto
  document.title = localStorage.getItem("project");
  document.getElementById("title").innerHTML = localStorage.getItem("project");
  
  //Preparar a pagina
  if (localStorage.getItem("html") == null) {
    addTab(nameDefault='Nivelamento');
    addTab(nameDefault='Contra-Nivelamento');
    showContentTab(document.getElementById("tabhelp"))
  }
  else document.getElementById("info").innerHTML = localStorage.getItem("html");
  
  window.user_location = {}
  window.user_bounds = {
    "SW": [90, 180],
    "NE": [-90, -180]
  }
}

function getRandomColor() {
  // Fonte: https://stackoverflow.com/questions/1484506/random-color-generator
  const chars = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += chars[Math.floor(Math.random() * chars.length)];
  }
  return color;
}
