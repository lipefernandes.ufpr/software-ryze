function addLocation($name) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                let latitude = position.coords.latitude,
                    longitude = position.coords.longitude;
                if (!($name in window.user_location)) window.user_location[$name] = [];
                window.user_location[$name].push([latitude, longitude]);
                if (window.user_bounds["SW"][0] > latitude - latitude * 0.0035) window.user_bounds["SW"][0] = latitude - latitude * 0.0035;
                if (window.user_bounds["NE"][0] < latitude + latitude * 0.0035) window.user_bounds["NE"][0] = latitude + latitude * 0.0035;
                if (window.user_bounds["SW"][1] > longitude - longitude * 0.0035) window.user_bounds["SW"][1] = longitude - longitude * 0.0035;
                if (window.user_bounds["NE"][1] < longitude + longitude * 0.0035) window.user_bounds["NE"][1] = longitude + longitude * 0.0035;
            }
        );
    }
    else alert("Não foi possível coletar a sua localização.");
}

function showPosition() {
    function highlightFeature(e) {e.target.setStyle({color: '#ffff00'});}
    function setBounds(mapa) {mapa.setMaxBounds(Object.values(window.user_bounds));}

    var map = new L.map(
        "map",
        {
            zoomControl: true,
            minZoom: 12
        }
    ).setView(
        [
            (window.user_bounds["SW"][0] + window.user_bounds["NE"][0]) / 2,
            (window.user_bounds["SW"][1] + window.user_bounds["NE"][1]) / 2
        ], 15
    );
    
    basemap = L.tileLayer(
        "http://a.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
        {
            opacity: 1.0,
            attribution: "Software Ryze | <a href='https://cartodb.com/basemaps/' title='Map tiles by CartoDB, under CC BY 3.0. Data by OpenStreetMap, under ODbL'>CartoDB</a>"
        }
    ).addTo(map);
    Object.entries(window.user_location).forEach(
        ([key, value]) => {
            polyline = L.polyline(
                value,
                {color: getRandomColor(), weight: 5.0}
            ).addTo(map);
            value.forEach(function (val) {
               L.marker(
                   val,
                   {icon: L.icon(
                       {
                           iconUrl: "https://www.shareicon.net/data/2015/07/09/66828_marker_256x256.png",
                           iconSize: [15, 15]
                        }
                   )},
                   {color: "red"}
                ).addTo(map); 
            })
        }
    );
    L.control.scale().addTo(map);
    setBounds(map);
}
