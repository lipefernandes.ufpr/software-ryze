function createProfile() {
    // Instanciar variáveis principais
    // CanvasJS.addColorSet(
    //     "Custom",
    //     [
    //         "#000"
    //     ]
    // );
    var chart = new CanvasJS.Chart(
        "perfil",
        {
            exportEnabled: true,
            culture: "pt-br",
            //colorSet: "Custom",
            theme: "light1",
            animationEnabled: true,
            animationDuration: 3000,
            title: {
                text: $("title").text()
            },
            axisX: {
                title: "Distância percorrida",
                valueFormatString: "0 m",
                minimum: 0
            },
            axisY: {
                valueFormatString: "0 m"
            },
            toolTip: {
                enabled: true
            },
            data: window.results,
        }
    );
    chart.render();
}
