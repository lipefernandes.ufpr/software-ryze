<h1>Caderneta</h1>
Esta página é voltada à inserção de dados, de modo a ser possível criar diversas cadernetas, renomeá-las de acordo com o desejo do usuário e, além disso há uma guia de ajuda, na qual apresenta todas as informações necessárias para preencher os dados. Basicamente esta página pode ser dividida em três partes:

- Abas de navegação: Aqui é possível visualizar e navegar por entre os levantamentos (abas). Além disso, é possível adicionar levantamentos ao clicar sobre o ícone “+” e visualizar onde as informações devem ser inseridas no ícone “?”. Além disso, é possível renomear as abas de acordo com o interesse do usuário, sabendo que este nome será utilizado na representação dos dados.
- Planilha: Este painel é uma tabela com inicialmente uma linha e 8 colunas de modo a serem preenchidas ou calculadas:
    - RE: Identificação do ponto de ré.
    - VANTE: Identificação do ponto de vante.
    - DISTANCIA RE: Distância calculada entre a posição ocupada e o ponto de ré.
    - LEITURA RE: Leituras dos fios estadimétricos (superior, médio e inferior) de ré, respectivamente, sendo que a leitura do fio médio deve ter o mesmo valor que a média dos valores superior e inferior, com uma tolerância de 2 milímetros.
    - LEITURA VANTE: Leituras dos fios estadimétricos (superior, médio e inferior) de vante, respectivamente, sendo que a leitura do fio médio deve ter o mesmo valor que a média dos valores superior e inferior, com uma tolerância de 2 milímetros.
    - DISTANCIA VANTE: Distância calculada entre a posição ocupada e o ponto de vante.
    - DESNIVEL: Desnível calculado entre os pontos de ré e vante.
    - ✔: Botão que realiza o cálculo das grandezas em questão, juntamente com as devidas verificações. Após a realização deste micro-processamento, a linha será travada, a fim de não haver corrupção nos dados e a única possibilidade de ação será apagar a linha criada. Além disso, ao clicar neste botão, o programa coleta a geolocalização do usuário em forma de coordenadas a fim de mapear o encaminhamento realizado.
Botão inferior: Botão que adiciona uma nova linha na planilha ativa do momento, fazendo com que seja possível inserir novas observações.

<h1>Resultados</h1>
Esta página de resultados, a qual contém o resultado do processamento de cada levantamento realizado, assim como os respectivos perfis e mapa de localização navegável. Para chegar nela, é necessário clicar sobre o ▶ no canto superior direto, o qual irá realizar o processamento de todas as linhas de cada levantamento, sendo que a localização não será afetada por este botão. Basicamente esta página pode ser dividida em três partes:

- Tabela de resultados: Uma tabela com o resultado do processamento de cada um dos levantamentos, com as informações:
    - Nome do levantamento
    - Número de observações
    - Distância média entre os lances
    - Distância total percorrida
    - Ponto inicial
    - Ponto final
    - Desnível total entre o ponto inicial e o ponto final
- Perfil: Gráfico onde é possível visualizar a nuância do levantamento, juntamente com os pontos observados através de um perfil do alinhamento seguido, de modo a cada levantamento apresentar uma cor diferente. Além disso, é possível interagir com os pontos ao longo das linhas de modo a visualizar o nome do ponto e a altitude, visalizar discrepâncias encontradas e exportar o resultado como PDF, PNG ou JPGE.
- Mapa: Mapa dos locais inseridos pela verificação manual, de modo a ser possível navegar pela região e visualizar o encaminhamento realizado.


